#include <iostream>
#include <vector>
#include "rdi_wfile.hpp"
#include "rdi_buckwalter.hpp"
using namespace std;

int main(int argc, char const *argv[])
{
	if (argc < 2)
	{
		return 0;
	}
	try
	{
		string input_path(argv[1]);
		vector<wstring> input_lines = RDI::read_wfile_lines(input_path);
		for (auto& line : input_lines)
		{
			auto out_lines = RDI::convert_arabic_to_buckwalter(line);
			RDI::convert_arabic_to_buckwalter_no_tashkeel(line);
			RDI::convert_arabic_to_arabic_without_tashkeel(line);
		}
		vector<string> input_lines_2 = RDI::read_file_lines(input_path);
		for (auto& line : input_lines_2)
		{
			RDI::convert_buckwalter_to_arabic(line);
			RDI::convert_buckwalter_to_arabic_no_tashkeel(line);
			RDI::remove_tashkeel_from_buckwalter(line);
		}
		//convert_buckwalter_to_arabic
	}
	catch (const std::exception& e)
	{
		cerr << e.what() << endl;
	}

	/* code */
	return 0;
}
