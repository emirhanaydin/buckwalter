TEMPLATE = app
CONFIG += console c++1z
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXX = afl-g++

INCLUDEPATH += /opt/rdi/include
CONFIG(release, debug|release) {
    LIBS += -L/opt/rdi/lib
}
CONFIG(debug, debug|release) {
    LIBS += -L/opt/rdi/lib_debug
}
LIBS += -lwfile -lbuckwalter -lstdc++fs

SOURCES += \
        main.cpp
