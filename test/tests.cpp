#include <iostream>
#include <string>

#include "catch.hpp"

#include "rdi_buckwalter.hpp"

using namespace std;
using namespace RDI;



TEST_CASE("convert_arabic_to_buckwalter")
{

	SECTION("Only Arabic Characters")
	{
		wstring ayah_arabic_no_tashkeel
			= L"محمد رسول الله والذين معه أشداء على الكفار رحماء بينهم تراهم "
			  L"ركعا سجدا يبتغون فضلا من الله ورضوانا سيماهم في وجوههم من أثر "
			  L"السجود ذلك مثلهم في التوراة ومثلهم في الإنجيل كزرع أخرج شطأه "
			  L"فآزره فاستغلظ فاستوى على سوقه يعجب الزراع ليغيظ بهم الكفار وعد "
			  L"الله الذين آمنوا وعملوا الصالحات منهم مغفرة وأجرا عظيما";

		string ayah_buck_actual =
				convert_arabic_to_buckwalter(ayah_arabic_no_tashkeel);
		string ayah_buck_expected
			= "mHmd rswl Allh wAl*yn mEh >$dA' ElY AlkfAr rHmA' bynhm trAhm "
			  "rkEA sjdA ybtgwn fDlA mn Allh wrDwAnA symAhm fy wjwhhm mn >vr "
			  "Alsjwd *lk mvlhm fy AltwrAp wmvlhm fy Al<njyl kzrE >xrj $T>h "
			  "f|zrh fAstglZ fAstwY ElY swqh yEjb AlzrAE lygyZ bhm AlkfAr wEd "
			  "Allh Al*yn |mnwA wEmlwA AlSAlHAt mnhm mgfrp w>jrA EZymA";

		REQUIRE(size(ayah_buck_actual) == size(ayah_buck_expected));
		REQUIRE(ayah_buck_actual == ayah_buck_expected );

		wstring ayah_arabic_tashkeel =
				   L"مُحَمَّدٌ رَّسُولُ اللَّهِ وَالَّذِينَ مَعَهُ أَشِدَّاءُ عَلَى الكُفَّارِ"
				   L" رُحَمَاءُ بَيْنَهُمْ تَرَاهُمْ رُكَّعاً سُجَّداً يَبْتَغُونَ فَضْلاً مِّنَ اللَّهِ "
				   L"وَرِضْوَاناً سِيمَاهُمْ فِي وَجُوهِهِم مِّنْ أَثَرِ السُّجُودِ ذَلِكَ "
				   L"مَثَلُهُمْ فِي التَّوْرَاةِ وَمَثَلُهُمْ فِي الإِنجِيلِ كَزَرْعٍ أَخْرَجَ "
				   L"شَطْأَهُ فَآزَرَهُ فَاسْتَغْلَظَ فَاسْتَوَى عَلَى سُوقِهِ يُعْجِبُ "
				   L"الزُّرَّاعَ لِيَغِيظَ بِهِمُ الكُفَّارَ وَعَدَ اللَّهُ الَّذِينَ آمَنُوا وَعَمِلُوا "
				   L"الصَّالِحَاتِ مِنْهُم مَّغْفِرَةً وَأَجْراً عَظِيماً ";

		ayah_buck_actual =
		convert_arabic_to_buckwalter(ayah_arabic_tashkeel);
		ayah_buck_expected =
				"muHam~adN r~asuwlu All~ahi waAl~a*iyna "
				"maEahu >a$id~aA'u EalaY Alkuf~aAri ruHamaA'u "
				"bayonahumo taraAhumo ruk~aEAF suj~adAF "
				"yabotaguwna faDolAF m~ina All~ahi wariDowaAnAF "
				"siymaAhumo fiy wajuwhihim m~ino >avari "
				"Als~ujuwdi *alika mavaluhumo fiy Alt~aworaApi "
				"wamavaluhumo fiy Al<injiyli kazaroEK >axoraja "
				"$aTo>ahu fa|zarahu faAsotagolaZa faAsotawaY "
				"EalaY suwqihi yuEojibu Alz~ur~aAEa liyagiyZa "
				"bihimu Alkuf~aAra waEada All~ahu Al~a*iyna "
				"|manuwA waEamiluwA AlS~aAliHaAti minohum "
				"m~agofirapF wa>ajorAF EaZiymAF ";

		REQUIRE(size(ayah_buck_actual) == size(ayah_buck_expected));
		REQUIRE(ayah_buck_actual == ayah_buck_expected );


	}

	SECTION("Only Arabic Characters with replacing unknowns")
	{
		wstring ayah_arabic_no_tashkeel
			= L"محمد رسول الله والذين معه أشداء على الكفار رحماء بينهم تراهم "
			  L"ركعا سجدا يبتغون فضلا من الله ورضوانا سيماهم في وجوههم من أثر "
			  L"السجود ذلك مثلهم في التوراة ومثلهم في الإنجيل كزرع أخرج شطأه "
			  L"فآزره فاستغلظ فاستوى على سوقه يعجب الزراع ليغيظ بهم الكفار وعد "
			  L"الله الذين آمنوا وعملوا الصالحات منهم مغفرة وأجرا عظيما";

		string ayah_buck_actual =
				convert_arabic_to_buckwalter(ayah_arabic_no_tashkeel,"xd");
		string ayah_buck_expected
			= "mHmd rswl Allh wAl*yn mEh >$dA' ElY AlkfAr rHmA' bynhm trAhm "
			  "rkEA sjdA ybtgwn fDlA mn Allh wrDwAnA symAhm fy wjwhhm mn >vr "
			  "Alsjwd *lk mvlhm fy AltwrAp wmvlhm fy Al<njyl kzrE >xrj $T>h "
			  "f|zrh fAstglZ fAstwY ElY swqh yEjb AlzrAE lygyZ bhm AlkfAr wEd "
			  "Allh Al*yn |mnwA wEmlwA AlSAlHAt mnhm mgfrp w>jrA EZymA";

		REQUIRE(size(ayah_buck_actual) == size(ayah_buck_expected));
		REQUIRE(ayah_buck_actual == ayah_buck_expected );

		ayah_arabic_no_tashkeel
			= L"محمد رسول الله والذين معه أشداء على ひらがなالكفار رحماء ZBLSGI3qBaبينهم تراهم "
			  L"ركعا سجدا يبتغون فضلا من الله ورضوانا سيماهم في وجوههم من أثر "
			  L"السجود ذلك مثلهم ZBLSGI3qBaفي التوراة ومثلهم في الإنجيل كزرع 半濁点,أخرج شطأه "
			  L"فآزره فاستغلظ 传/傳فاستوى على سوقه يعجب ZBLSGI3qBaالزراع ليغيظ بهم الكفار وعد "
			  L"الله الذين 折آمنوا وعملواZBLSGI3qBa الصالحات ZBLSGI3qBaمنهم مغفرة وأجرا عظيما";

		ayah_buck_actual =
				convert_arabic_to_buckwalter(ayah_arabic_no_tashkeel,"");
		ayah_buck_expected
			= "mHmd rswl Allh wAl*yn mEh >$dA' ElY AlkfAr rHmA' bynhm trAhm "
			  "rkEA sjdA ybtgwn fDlA mn Allh wrDwAnA symAhm fy wjwhhm mn >vr "
			  "Alsjwd *lk mvlhm fy AltwrAp wmvlhm fy Al<njyl kzrE >xrj $T>h "
			  "f|zrh fAstglZ fAstwY ElY swqh yEjb AlzrAE lygyZ bhm AlkfAr wEd "
			  "Allh Al*yn |mnwA wEmlwA AlSAlHAt mnhm mgfrp w>jrA EZymA";
		REQUIRE(size(ayah_buck_actual) == size(ayah_buck_expected));
		REQUIRE(ayah_buck_actual == ayah_buck_expected );


		wstring ayah_arabic_tashkeel =
				   L"مُحَمَّدٌ رَّسُولُ اللَّهِ وَالَّذِينَ مَعَهُ 半濁点,أَشِدَّاءُ عَلَى الكُفَّارِ"
				   L"رُحَمَاءُ بَيْنَهُمْ تَرَاهُمْ رُكَّعاً 半濁点,سُجَّداً يَبْتَغُونَ فَضْअلاً مِّنَ اللَّهِ "
				   L"وَرِضْوَاناً سِيمَاهُمْ فِي وَجُوهِهِم مِّنْ أَثَرِ السُّجُऊودِ ذَلِكَ "
				   L"مَثَلُهُمْ فِي التَّوْرَاةِ وَمَثَلُهُمْ فِيaäāäʲaʲaᵐeɛiīouūɯɯ̄ ̌الإِنجِيلِ كَزَرْعٍ أَخْرَجَ "
				   L"شَطْأَهُ فَآزَرَهُ فَاسْتَغْلَظَ فَاسْتَوَى عَلَअى سُوقِهِ يُعْجِبُ "
				   L"الزُّرَّاعَ لِيَغِيظَ بِهِمُ الكُفَّارَ وَعَدَ اللَّهُ الَّذِينَ آمَنُوا وَعَمِلُوا "
				   L"الصَّالِحَاتِ مِنْهُم مَّغْفِرَةً وَأَجْراً عَظِيماً) ";


		ayah_buck_actual =
		convert_arabic_to_buckwalter(ayah_arabic_tashkeel,"");
		ayah_buck_expected =
				"muHam~adN r~asuwlu All~ahi waAl~a*iyna "
				"maEahu >a$id~aA'u EalaY Alkuf~aAriruHamaA'u "
				"bayonahumo taraAhumo ruk~aEAF suj~adAF "
				"yabotaguwna faDolAF m~ina All~ahi wariDowaAnAF "
				"siymaAhumo fiy wajuwhihim m~ino >avari "
				"Als~ujuwdi *alika mavaluhumo fiy Alt~aworaApi "
				"wamavaluhumo fiy Al<injiyli kazaroEK >axoraja "
				"$aTo>ahu fa|zarahu faAsotagolaZa faAsotawaY "
				"EalaY suwqihi yuEojibu Alz~ur~aAEa liyagiyZa "
				"bihimu Alkuf~aAra waEada All~ahu Al~a*iyna "
				"|manuwA waEamiluwA AlS~aAliHaAti minohum "
				"m~agofirapF wa>ajorAF EaZiymAF ";
		REQUIRE(size(ayah_buck_actual) == size(ayah_buck_expected));
		REQUIRE(ayah_buck_actual == ayah_buck_expected );

		ayah_arabic_tashkeel =
				   L"مُحَمَّدٌ رَّسُولُ اللَّهِ وَالَّذِينَ مَعَهُ 半濁点,أَشِدَّاءُ عَلَى الكُفَّارِ";

		ayah_buck_actual =
		convert_arabic_to_buckwalter(ayah_arabic_tashkeel,"abook");

		ayah_buck_expected =
				"muHam~adN r~asuwlu All~ahi waAl~a*iyna "
				"maEahu abookabookabookabook>a$id~aA'u "
				"EalaY Alkuf~aAri";

		REQUIRE(size(ayah_buck_actual) == size(ayah_buck_expected));
		REQUIRE(ayah_buck_actual == ayah_buck_expected );

	}


}

TEST_CASE("convert_arabic_to_buckwalter_no_tashkeel")
{
	wstring ayah_arabic_no_tashkeel
		= L"محمد رسول الله والذين معه أشداء على الكفار رحماء بينهم تراهم "
		  L"ركعا سجدا يبتغون فضلا من الله ورضوانا سيماهم في وجوههم من أثر "
		  L"السجود ذلك مثلهم في التوراة ومثلهم في الإنجيل كزرع أخرج شطأه "
		  L"فآزره فاستغلظ فاستوى على سوقه يعجب الزراع ليغيظ بهم الكفار وعد "
		  L"الله الذين آمنوا وعملواdd الصالحات منهم مغفرة وأجرا عظيما";

	string ayah_buck_actual =
			convert_arabic_to_buckwalter_no_tashkeel(ayah_arabic_no_tashkeel);
	string ayah_buck_expected
		= "mHmd rswl Allh wAl*yn mEh >$dA' ElY AlkfAr rHmA' bynhm trAhm "
		  "rkEA sjdA ybtgwn fDlA mn Allh wrDwAnA symAhm fy wjwhhm mn >vr "
		  "Alsjwd *lk mvlhm fy AltwrAp wmvlhm fy Al<njyl kzrE >xrj $T>h "
		  "f|zrh fAstglZ fAstwY ElY swqh yEjb AlzrAE lygyZ bhm AlkfAr wEd "
		  "Allh Al*yn |mnwA wEmlwA AlSAlHAt mnhm mgfrp w>jrA EZymA";

	REQUIRE(size(ayah_buck_actual) == size(ayah_buck_expected));
	REQUIRE(ayah_buck_actual == ayah_buck_expected );

	wstring ayah_arabic_tashkeel =
			   L"مُحَمَّدٌ رَّسُولُ اللَّهِ وَالَّذِينَ مَعَهُ أَشِدَّاءُ عَلَى الكُفَّارِ "
			   L"رُحَمَاءُ بَيْنَهُمْ تَرَاهُمْ رُكَّعاً سُجَّداً يَبْتَغُونَ فَضْلاً مِّنَ اللَّهِ "
			   L"وَرِضْوَاناً سِيمَاهُمْ فِي وَجُوهِهِم مِّنْ أَثَرِ السُّجُودِ ذَلِكَ "
			   L"مَثَلُهُمْ فِي التَّوْرَاةِ وَمَثَلُهُمْ فِي الإِنجِيلِ كَزَرْعٍ أَخْرَجَ "
			   L"شَطْأَهُ فَآزَرَهُ فَاسْتَغْلَظَ فَاسْتَوَى عَلَى سُوقِهِ يُعْجِبُ "
			   L"الزُّرَّاعَ لِيَغِيظَ بِهِمُ الكُفَّارَ وَعَدَ اللَّهُ الَّذِينَ آمَنُوا وَعَمِلُوا "
			   L"الصَّالِحَاتِ مِنْهُم مَّغْفِرَةً وَأَجْراً عَظِيماً";

	ayah_buck_actual =
	convert_arabic_to_buckwalter_no_tashkeel(ayah_arabic_tashkeel,"wow");

	REQUIRE(size(ayah_buck_actual) == size(ayah_buck_expected));
	REQUIRE(ayah_buck_actual == ayah_buck_expected );

	wstring arabic_sentence =
			L"صِفْ خَلْقَ خَوْدٍ كمثل الشمس إذ بَزَغَتْ *** يَحْـظى الضَّجيـع بها نجـلاءُ معـطارُ";

	string buck_expected =
			"Sf xlq xwd kmvl Al$ms <* bzgt wowwowwow yHZY AlDjyE bhA njlA' mETAr";
	string buck_actual =
			convert_arabic_to_buckwalter_no_tashkeel(arabic_sentence,"wow");
	REQUIRE(size(ayah_buck_actual) == size(ayah_buck_expected));
	REQUIRE(ayah_buck_actual == ayah_buck_expected );
}




TEST_CASE("convert_buckwalter_to_arabic")
{
	string ayah_buck
			= "mHmd rswl Allh wAl*yn mEh >$dA' ElY AlkfAr rHmA' bynhm trAhm "
			  "rkEA sjdA ybtgwn fDlA mn Allh wrDwAnA symAhm fy wjwhhm mn >vr "
			  "Alsjwd *lk mvlhm fy AltwrAp wmvlhm fy Al<njyl kzrE >xrj $T>h "
			  "f|zrh fAstglZ fAstwY ElY swqh yEjb AlzrAE lygyZ bhm AlkfAr wEd "
			  "Allh Al*yn |mnwA wEmlwA AlSAlHAt mnhm mgfrp w>jrA EZymA";

	 wstring ayah_arabic_expected
			= L"محمد رسول الله والذين معه أشداء على الكفار رحماء بينهم تراهم "
			  L"ركعا سجدا يبتغون فضلا من الله ورضوانا سيماهم في وجوههم من أثر "
			  L"السجود ذلك مثلهم في التوراة ومثلهم في الإنجيل كزرع أخرج شطأه "
			  L"فآزره فاستغلظ فاستوى على سوقه يعجب الزراع ليغيظ بهم الكفار وعد "
			  L"الله الذين آمنوا وعملوا الصالحات منهم مغفرة وأجرا عظيما";

	 wstring ayah_arabic_actual =
			 convert_buckwalter_to_arabic(ayah_buck);


	 REQUIRE(size(ayah_arabic_actual) == size(ayah_arabic_expected));
	 REQUIRE(ayah_arabic_actual == ayah_arabic_expected );

	 ayah_buck =
			 "muHam~adN r~asuwlu All~ahi waAl~a*iyna "
			 "maEahu >a$id~aA'u EalaY Alkuf~aAriruHamaA'u "
			 "bayonahumo taraAhumo ruk~aEAF suj~adAF "
			 "yabotaguwna faDolAF m~ina All~ahi wariDowaAnAF "
			 "siymaAhumo fiy wajuwhihim m~ino >avari "
			 "Als~ujuwdi *alika mavaluhumo fiy Alt~aworaApi "
			 "wamavaluhumo fiy Al<injiyli kazaroEK >axoraja "
			 "$aTo>ahu fa|zarahu faAsotagolaZa faAsotawaY "
			 "EalaY suwqihi yuEojibu Alz~ur~aAEa liyagiyZa "
			 "bihimu Alkuf~aAra waEada All~ahu Al~a*iyna "
			 "|manuwA waEamiluwA AlS~aAliHaAti minohum "
			 "m~agofirapF wa>ajorAF EaZiymAF ";

	 ayah_arabic_expected =
			 L"مُحَمَّدٌ رَّسُولُ اللَّهِ وَالَّذِينَ مَعَهُ أَشِدَّاءُ عَلَى الكُفَّارِ"
			 L"رُحَمَاءُ بَيْنَهُمْ تَرَاهُمْ رُكَّعاً سُجَّداً يَبْتَغُونَ فَضْلاً مِّنَ اللَّهِ "
			 L"وَرِضْوَاناً سِيمَاهُمْ فِي وَجُوهِهِم مِّنْ أَثَرِ السُّجُودِ ذَلِكَ "
			 L"مَثَلُهُمْ فِي التَّوْرَاةِ وَمَثَلُهُمْ فِي الإِنجِيلِ كَزَرْعٍ أَخْرَجَ "
			 L"شَطْأَهُ فَآزَرَهُ فَاسْتَغْلَظَ فَاسْتَوَى عَلَى سُوقِهِ يُعْجِبُ "
			 L"الزُّرَّاعَ لِيَغِيظَ بِهِمُ الكُفَّارَ وَعَدَ اللَّهُ الَّذِينَ آمَنُوا وَعَمِلُوا "
			 L"الصَّالِحَاتِ مِنْهُم مَّغْفِرَةً وَأَجْراً عَظِيماً ";


	 ayah_arabic_actual =
		convert_buckwalter_to_arabic(ayah_buck);
	 REQUIRE(size(ayah_arabic_actual) == size(ayah_arabic_expected));
	 REQUIRE(ayah_arabic_actual == ayah_arabic_expected );
}


TEST_CASE("convert_buckwalter_to_arabic_no_tashkeel")
{

	string ayah_buck
			= "mHmd rswl Allh wAl*yn mEh >$dA' ElY AlkfAr rHmA' bynhm trAhm "
			  "rkEA sjdA ybtgwn fDlA mn Allh wrDwAnA symAhm fy wjwhhm mn >vr "
			  "Alsjwd *lk mvlhm fy AltwrAp wmvlhm fy Al<njyl kzrE >xrj $T>h "
			  "f|zrh fAstglZ fAstwY ElY swqh yEjb AlzrAE lygyZ bhm AlkfAr wEd "
			  "Allh Al*yn |mnwA wEmlwA AlSAlHAt mnhm mgfrp w>jrA EZymA";

	 wstring ayah_arabic_expected
			= L"محمد رسول الله والذين معه أشداء على الكفار رحماء بينهم تراهم "
			  L"ركعا سجدا يبتغون فضلا من الله ورضوانا سيماهم في وجوههم من أثر "
			  L"السجود ذلك مثلهم في التوراة ومثلهم في الإنجيل كزرع أخرج شطأه "
			  L"فآزره فاستغلظ فاستوى على سوقه يعجب الزراع ليغيظ بهم الكفار وعد "
			  L"الله الذين آمنوا وعملوا الصالحات منهم مغفرة وأجرا عظيما";

	 wstring ayah_arabic_actual =
			 convert_buckwalter_to_arabic_no_tashkeel(ayah_buck);


	 REQUIRE(size(ayah_arabic_actual) == size(ayah_arabic_expected));
	 REQUIRE(ayah_arabic_actual == ayah_arabic_expected );

	 ayah_buck =
			 "muHam~adN r~asuwlu All~ahi waAl~a*iyna "
			 "maEahu >a$id~aA'u EalaY Alkuf~aAri ruHamaA'u "
			 "bayonahumo taraAhumo ruk~aEAF suj~adAF "
			 "yabotaguwna faDolAF m~ina All~ahi wariDowaAnAF "
			 "siymaAhumo fiy wajuwhihim m~ino >avari "
			 "Als~ujuwdi *alika mavaluhumo fiy Alt~aworaApi "
			 "wamavaluhumo fiy Al<injiyli kazaroEK >axoraja "
			 "$aTo>ahu fa|zarahu faAsotagolaZa faAsotawaY "
			 "EalaY suwqihi yuEojibu Alz~ur~aAEa liyagiyZa "
			 "bihimu Alkuf~aAra waEada All~ahu Al~a*iyna "
			 "|manuwA waEamiluwA AlS~aAliHaAti minohum "
			 "m~agofirapF wa>ajorAF EaZiymAF";


	 ayah_arabic_actual =
		convert_buckwalter_to_arabic_no_tashkeel(ayah_buck);


	 REQUIRE(size(ayah_arabic_actual) == size(ayah_arabic_expected));
	 REQUIRE(ayah_arabic_actual == ayah_arabic_expected );



	 ayah_buck =
			 "muHam~半濁点adN r~asuwlu All~ahi waAl~a*iyna "
			 "maEahu >a$id~aA'u EalaY Alkuf~aAri ruHamaA'u "
			 "bayonahumo taraAhumo ruk~aEAF suj~adAF "
			 "yabotaguwna faDolAF m~ina All~ahi wariDowaAnAF "
			 "siymaAhumo fiy wajuwhihim半濁点 m~ino >avari "
			 "Als~ujuwdiهاهاهاها *alika mavaluhumo fiy Alt~aworaApi "
			 "wamavaluhumo fiy Al<injiyli kazaroEK >axoraja "
			 "$aTo>ahu fa|zarahu faAsotagolaZa faAsotawaY "
			 "EalaY suwqihi 半濁点yuEojibu Alz~ur~aAEa liyagiyZa "
			 "bihimu Alkuf~aAra waEada All~ahu حيوانAl~a*iyna "
			 "|manuwA waEamiluwA AlS~aAliHaAti minohum "
			 "半濁点m~agofirapF wa>ajorAF EaZiymAF半濁点半濁点";


	 ayah_arabic_actual =
		convert_buckwalter_to_arabic_no_tashkeel(ayah_buck);


	 REQUIRE(size(ayah_arabic_actual) == size(ayah_arabic_expected));
	 REQUIRE(ayah_arabic_actual == ayah_arabic_expected );

}




TEST_CASE("convert_arabic_to_arabic_without_tashkeel")
{
	wstring ayah_arabic
			= L"محمد رسول الله والذين معه أشداء على الكفار رحماء بينهم تراهم "
			  L"ركعا سجدا يبتغون فضلا مf3ن الله ورضوانا سيماهم في وجوههم من أثر "
			  L"السجود ذلك مثلdهم في التوراة ومثلهم فيd الإنجيل كزرع أخرج شطأه "
			  L"فآزره فاستغلظ فاستوى علىf سوقه يعجب الزراع ليغيظ بهم الكفار وعد "
			  L"الله الذين آمنوا وعملوا الصاdfلحvات منهم مغفرة وأجرا عظيما";


	 wstring ayah_arabic_expected
			= L"محمد رسول الله والذين معه أشداء على الكفار رحماء بينهم تراهم "
			  L"ركعا سجدا يبتغون فضلا من الله ورضوانا سيماهم في وجوههم من أثر "
			  L"السجود ذلك مثلهم في التوراة ومثلهم في الإنجيل كزرع أخرج شطأه "
			  L"فآزره فاستغلظ فاستوى على سوقه يعجب الزراع ليغيظ بهم الكفار وعد "
			  L"الله الذين آمنوا وعملوا الصالحات منهم مغفرة وأجرا عظيما";

	 wstring ayah_arabic_actual =
			 convert_arabic_to_arabic_without_tashkeel(ayah_arabic);
	  REQUIRE(size(ayah_arabic_actual) == size(ayah_arabic_expected));
	 REQUIRE(ayah_arabic_actual == ayah_arabic_expected );



	 ayah_arabic =
			 L"مُحَمَّدٌ رَّسُولُ اللَّهِ وَالَّذِينَ مَعَهُ أَشِدَّاءُ عَلَى الكُفَّارِ"
			 L" رُحَمَاءُ بَيْنَهُمْ تَرَاهُمْ رُكَّعاً سُجَّداً يَبْتَغُونَ فَضْلاً مِّنَ اللَّهِ "
			 L"وَرِضْوَاناً سِيمَاهُمْ فِي وَجُوهِهِم مِّنْ أَثَرِ السُّجُودِ ذَلِكَ "
			 L"مَثَلُهُمْ فِي التَّوْرَاةِ وَمَثَلُهُمْ فِي الإِنجِيلِ كَزَرْعٍ أَخْرَجَ "
			 L"شَطْأَهُ فَآزَرَهُ فَاسْتَغْلَظَ فَاسْتَوَى عَلَى سُوقِهِ يُعْجِبُ "
			 L"الزُّرَّاعَ لِيَغِيظَ بِهِمُ الكُفَّارَ وَعَدَ اللَّهُ الَّذِينَ آمَنُوا وَعَمِلُوا "
			 L"الصَّالِحَاتِ مِنْهُم مَّغْفِرَةً وَأَجْراً عَظِيماً";
	 ayah_arabic_actual =
		convert_arabic_to_arabic_without_tashkeel(ayah_arabic);


	 REQUIRE(size(ayah_arabic_actual) == size(ayah_arabic_expected));
	 REQUIRE(ayah_arabic_actual == ayah_arabic_expected );
}

TEST_CASE("remove_tashkeel_from_buckwalter")
{
	string ayah_buck =
			"muHam~adN r~asuwlu All~ahi waAl~a*iyna "
			"maEahu >a$id~aA'u EalaY Alkuf~aAri ruHamaA'u "
			"bayonahumo taraAhumo ruk~aEAF suj~adAF "
			"yabotaguwna faDolAF m~ina All~ahi wariDowaAnAF "
			"siymaAhumo fiy wajuwhihim m~ino >avari "
			"Als~ujuwdi *alika mavaluhumo fiy Alt~aworaApi "
			"wamavaluhumo fiy Al<injiyli kazaroEK >axoraja "
			"$aTo>ahu fa|zarahu faAsotagolaZa faAsotawaY "
			"EalaY suwqihi yuEojibu Alz~ur~aAEa liyagiyZa "
			"bihimu Alkuf~aAra waEada All~ahu Al~a*iyna "
			"|manuwA waEamiluwA AlS~aAliHaAti minohum "
			"m~agofirapF wa>ajorAF EaZiymAF";

	string ayah_buck_expected
		= "mHmd rswl Allh wAl*yn mEh >$dA' ElY AlkfAr rHmA' bynhm trAhm "
		  "rkEA sjdA ybtgwn fDlA mn Allh wrDwAnA symAhm fy wjwhhm mn >vr "
		  "Alsjwd *lk mvlhm fy AltwrAp wmvlhm fy Al<njyl kzrE >xrj $T>h "
		  "f|zrh fAstglZ fAstwY ElY swqh yEjb AlzrAE lygyZ bhm AlkfAr wEd "
		  "Allh Al*yn |mnwA wEmlwA AlSAlHAt mnhm mgfrp w>jrA EZymA";

	string ayah_buck_actual = remove_tashkeel_from_buckwalter(ayah_buck);
	REQUIRE(size(ayah_buck_actual) == size(ayah_buck_expected));
	REQUIRE(ayah_buck_actual == ayah_buck_expected );

}
