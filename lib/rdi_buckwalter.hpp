#ifndef BUCKWALTER_H
#define BUCKWALTER_H

#include <map>
#include <vector>

/// \file rdi_buckwalter.h
/// @brief Everything buckwalter related.
/// If all you need is the below macro definitions for the Arabic
/// alphabet then you can just include this header.
/// You don't need to link to the library.
/// However if you're interested in the definitions of the maps, vectors and
/// conversion functions then you must link your program to this library

// HAMZA             -> 	'\''
// ALEF_MADDA        -> 	'|'
// ALEF_HAMZA_ABOVE  -> 	'>'
// WAW_HAMZA_ABOVE   -> 	'&'
// ALEF_HAMZA_BELOW  -> 	'<'
// YEH_HAMZA_ABOVE   -> 	'}'
// ALEF_NO_HAMZA     -> 	'A'
// BEH               -> 	'b'
// TEH_MARBOOTA      -> 	'p'
// TEH               -> 	't'
// THEH              -> 	'v'
// JEEM              -> 	'j'
// HAH               -> 	'H'
// KHAH              -> 	'x'
// DAL               -> 	'd'
// THAL              -> 	'*'
// REH               -> 	'r'
// ZAIN              -> 	'z'
// SEEN              -> 	's'
// SHEEN             -> 	'$'
// SAD               -> 	'S'
// DAD               -> 	'D'
// TAH               -> 	'T'
// ZAH               -> 	'Z'
// AIN               -> 	'E'
// GHAIN             -> 	'g'
// FEH               -> 	'f'
// QAF               -> 	'q'
// KAF               -> 	'k'
// LAM               -> 	'l'
// MEEM              -> 	'm'
// NOON              -> 	'n'
// HEH               -> 	'h'
// WAW               -> 	'w'
// YEH_NO_DOTS       -> 	'Y'
// YEH_WITH_DOTS     -> 	'y'
// TATWEEL           -> 	'_'
// TANWEEN_FATHA     -> 	'F'
// TANWEEN_DAMA      -> 	'N'
// TANWEEN_KASRA     -> 	'K'
// FATHA             -> 	'a'
// DAMA              -> 	'u'
// KASRA             -> 	'i'
// SHADA             -> 	'~'
// SKOON             -> 	'o'

// numbers
#define SEFR  L'\u0660'
#define WAHED  L'\u0661'
#define ETHNAN  L'\u0662'
#define THALATHA  L'\u0663'
#define ARBA3A  L'\u0664'
#define KHAMSA  L'\u0665'
#define SETTA  L'\u0666'
#define SAB3A  L'\u0667'
#define THAMANEYA  L'\u0668'
#define TES3A  L'\u0669'


// symbols
#define FASLA L'\u060C'
#define QUESTION_MARK L'\u061F'
#define FASLA_MANKOOTA L'\u060B'
#define NOKTATAN L'\u003A'
#define NESBA L'\u0025'
#define TA3AGOB L'\u0021'
#define KAWS_MAFTOOH L'\u0028'
#define KAWS_MAKFOOL L'\u0029'
#define DARB L'\u002A'
#define GAM3 L'\u002B'
#define KESMA L'\u002F'
#define TAR7 L'\u002D'
#define YOSAWI L'\u003D'
#define NOKTA L'\u06F0'

// harakat
#define TATWEEL L'\u0640'
#define TANWEEN_FATHA L'\u064b'
#define TANWEEN_DAMA L'\u064c'
#define TANWEEN_KASRA L'\u064d'
#define FATHA L'\u064e'
#define DAMA L'\u064f'
#define KASRA L'\u0650'
#define SHADA L'\u0651'
#define SKOON L'\u0652'

// alphabet
#define HAMZA L'\u0621'
#define ALEF_MADDA L'\u0622'
#define ALEF_HAMZA_ABOVE L'\u0623'
#define WAW_HAMZA_ABOVE L'\u0624'
#define ALEF_HAMZA_BELOW L'\u0625'
#define YEH_HAMZA_ABOVE L'\u0626'
#define ALEF_NO_HAMZA L'\u0627'
#define BEH L'\u0628'
#define TEH_MARBOOTA L'\u0629'
#define TEH L'\u062A'
#define THEH L'\u062B'
#define JEEM L'\u062C'
#define HAH L'\u062D'
#define KHAH L'\u062E'
#define DAL L'\u062F'
#define THAL L'\u0630'
#define REH L'\u0631'
#define ZAIN L'\u0632'
#define SEEN L'\u0633'
#define SHEEN L'\u0634'
#define SAD L'\u0635'
#define DAD L'\u0636'
#define TAH L'\u0637'
#define ZAH L'\u0638'
#define AIN L'\u0639'
#define GHAIN L'\u063A'
#define FEH L'\u0641'
#define QAF L'\u0642'
#define KAF L'\u0643'
#define LAM L'\u0644'
#define MEEM L'\u0645'
#define NOON L'\u0646'
#define HEH L'\u0647'
#define WAW L'\u0648'
#define YEH_NO_DOTS L'\u0649'
#define YEH_WITH_DOTS L'\u064A'



/// same thing but strings instead of chars
/// often used when you're trying to compose a string with the
/// + operator
/// example: S_ALEF_HAMZA_ABOVE + S_HEH + S_LAM + S_ALEF_NO_HAMZA +
/// S_TANWEEN_FATHA which reads "ahlan" in arabic


// numbers
#define S_SEFR std::wstring(L"\u0660")
#define S_WAHED std::wstring(L"\u0661")
#define S_ETHNAN std::wstring(L"\u0662")
#define S_THALATHA std::wstring(L"\u0663")
#define S_ARBA3A std::wstring(L"\u0664")
#define S_KHAMSA std::wstring(L"\u0665")
#define S_SETTA std::wstring(L"\u0666")
#define S_SAB3A std::wstring(L"\u0667")
#define S_THAMANEYA std::wstring(L"\u0668")
#define S_TES3A std::wstring(L"\u0669")

// symbols
#define S_FASLA std::wstring(L"\u060C")
#define S_QUESTION_MARK std::wstring(L"\u061F")
#define S_FASLA_MANKOOTA std::wstring(L"\u061B")
#define S_NOKTATAN std::wstring(L"\u003A")
#define S_NESBA std::wstring(L"\u066A")
#define S_TA3AGOB std::wstring(L"\u0021")
#define S_KAWS_MAFTOOH std::wstring(L"\u0028")
#define S_KAWS_MAKFOOL std::wstring(L"\u0029")
#define S_DARB std::wstring(L"\u002A")
#define S_GAM3 std::wstring(L"\u002B")
#define S_KESMA std::wstring(L"\u002F")
#define S_TAR7 std::wstring(L"\u002D")
#define S_YOSAWI std::wstring(L"\u003D")
#define S_NOKTA std::wstring(L"\u06F0")

#define S_TANSEES std::wstring(L"\u0022")
#define S_DARB std::wstring(L"\u002A")


// harakat
#define S_TATWEEL std::wstring(L"\u0640")
#define S_TANWEEN_FATHA std::wstring(L"\u064b")
#define S_TANWEEN_DAMA std::wstring(L"\u064c")
#define S_TANWEEN_KASRA std::wstring(L"\u064d")
#define S_FATHA std::wstring(L"\u064e")
#define S_DAMA std::wstring(L"\u064f")
#define S_KASRA std::wstring(L"\u0650")
#define S_SHADA std::wstring(L"\u0651")
#define S_SKOON std::wstring(L"\u0652")

// alphabet
#define S_HAMZA std::wstring(L"\u0621")
#define S_ALEF_MADDA std::wstring(L"\u0622")
#define S_ALEF_HAMZA_ABOVE std::wstring(L"\u0623")
#define S_WAW_HAMZA_ABOVE std::wstring(L"\u0624")
#define S_ALEF_HAMZA_BELOW std::wstring(L"\u0625")
#define S_YEH_HAMZA_ABOVE std::wstring(L"\u0626")
#define S_ALEF_NO_HAMZA std::wstring(L"\u0627")
#define S_BEH std::wstring(L"\u0628")
#define S_TEH_MARBOOTA std::wstring(L"\u0629")
#define S_TEH std::wstring(L"\u062A")
#define S_THEH std::wstring(L"\u062B")
#define S_JEEM std::wstring(L"\u062C")
#define S_HAH std::wstring(L"\u062D")
#define S_KHAH std::wstring(L"\u062E")
#define S_DAL std::wstring(L"\u062F")
#define S_THAL std::wstring(L"\u0630")
#define S_REH std::wstring(L"\u0631")
#define S_ZAIN std::wstring(L"\u0632")
#define S_SEEN std::wstring(L"\u0633")
#define S_SHEEN std::wstring(L"\u0634")
#define S_SAD std::wstring(L"\u0635")
#define S_DAD std::wstring(L"\u0636")
#define S_TAH std::wstring(L"\u0637")
#define S_ZAH std::wstring(L"\u0638")
#define S_AIN std::wstring(L"\u0639")
#define S_GHAIN std::wstring(L"\u063A")
#define S_FEH std::wstring(L"\u0641")
#define S_QAF std::wstring(L"\u0642")
#define S_KAF std::wstring(L"\u0643")
#define S_LAM std::wstring(L"\u0644")
#define S_MEEM std::wstring(L"\u0645")
#define S_NOON std::wstring(L"\u0646")
#define S_HEH std::wstring(L"\u0647")
#define S_WAW std::wstring(L"\u0648")
#define S_YEH_NO_DOTS std::wstring(L"\u0649")
#define S_YEH_WITH_DOTS std::wstring(L"\u064A")

namespace RDI
{

extern const std::map<wchar_t, char> arabic_to_buckwalter;
extern const std::map<char, wchar_t> buckwalter_to_arabic;
extern const std::vector<char> buckwalter_tashkeel;
extern const std::vector<char> buckwalter_letters_with_tashkeel;
extern const std::vector<char> buckwalter_letters_without_tashkeel;
extern const std::vector<char> char_spaces;
extern const std::vector<wchar_t> wspaces;
extern const std::map<char, wchar_t> space_to_wspace;
extern const std::map<wchar_t, char> wspace_to_space;
extern const std::vector<wchar_t> _arabic_letters_with_tashkeel;
extern const std::vector<wchar_t> _arabic_letters_without_tashkeel;
extern const std::vector<wchar_t> _arabic_tashkeel;

bool is_tashkeel(wchar_t c);
bool is_tashkeel(char c);
bool is_arabic_letter(wchar_t c);
bool is_arabic_letter(char c);
bool is_arabic_letter_or_tashkeel(wchar_t c);
bool is_arabic_letter_or_tashkeel(char c);
bool is_space(char c);
bool is_space(wchar_t c);

/// @brief Converts Arabic to Buckwalter
/// Any non-arab characters will be completely ignored and
/// the resulting buckwalter string will not contain them
/// tashkeel will be kept
std::string convert_arabic_to_buckwalter(const std::wstring& arabic,
										 const std::string& replace_unknown_with
										 = "");

/// @brief Converts Arabic to Buckwalter
/// Any non-arab characters or tashkeel will be completely ignored and
/// the resulting buckwalter string will not contain them
std::string convert_arabic_to_buckwalter_no_tashkeel(
	const std::wstring& arabic, const std::string& replace_unknown_with = "");

/// @brief Converts Buckwalter to Arabic
/// If given any letters that are not defined
/// in the buckwalter specification they
/// will be erased from the resulting wstring
std::wstring convert_buckwalter_to_arabic(const std::string& buckwalter);

/// @brief Converts Buckwalter to Arabic
/// If given any tashkeel or letter that is not defined
/// in the buckwalter specification
/// will be erased from the resulting wstring
std::wstring
convert_buckwalter_to_arabic_no_tashkeel(const std::string& buckwalter);

/// @brief Removes tashkeel from an arabic wstring
/// Any unknown character will be removed
std::wstring convert_arabic_to_arabic_without_tashkeel(
	const std::wstring& arabic_with_tashkeel);

/// @brief Removes tashkeel from a buckwalter string
std::string remove_tashkeel_from_buckwalter(const std::string& buckwalter);
} // namespace RDI

#endif // BUCKWALTER_H
