TEMPLATE = lib
CONFIG += staticlib c++1z
CONFIG -= qt
QMAKE_CXXFLAGS += -fopenmp -std=c++17
TARGET = buckwalter

SOURCES += buckwalter_conversions.cpp \
    buckwalter_transliteration.cpp \
    ar_letter_definitions.cpp

INCLUDEPATH += /opt/rdi/include

HEADERS += \
    rdi_buckwalter.hpp
